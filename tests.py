__author__ = 'tpaulus'

import json
import unittest

import digest


class TestConfig(unittest.TestCase):
    def load_file(self):
        with open('./email_config.json') as config_file:
            config_json = json.loads(config_file.read())
        return config_json

    def test_load_file(self):
        try:
            with open('./email_config.json') as config_file:
                config_json = json.loads(config_file.read())
            self.assertTrue(len(config_json) > 0)

        except Exception, e:
            self.fail(e)

    # noinspection PyStatementEffect
    def test_check_parameters(self):
        config_file = self.load_file()

        try:
            t = type(config_file['IMAP Login']['username'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Username in Config File')
        try:
            t = type(config_file['IMAP Login']['password'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Password in Config File')
        try:
            t = type(config_file['IMAP Login']['url'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No IMAP URL in Config File')

        try:
            self.assertTrue(len(config_file['Folders']) > 0)
            for folder in config_file['Folders']:
                try:
                    t = type(folder['Name'])
                    self.assertTrue(t == str or t == unicode)
                except KeyError:
                    self.fail('No Folder Name defined')
                try:
                    t = type(folder['Digest'])
                    self.assertTrue(t == bool)
                except KeyError:
                    self.fail('No Digest State defined for Folder: %s' % folder['Name'])
                try:
                    t = type(folder['Digest After'])
                    self.assertTrue(t == int)
                except KeyError:
                    self.fail('No Digest Age defined for Folder: %s' % folder['Name'])
        except KeyError:
            self.fail('No Folders to Digest in Config File')

        try:
            t = type(config_file['Send Email']['Sendgrid']['API Key'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No API Key for SendGrid in Config File')

        try:
            t = type(config_file['Send Email']['From Email'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Sender Email in Config File')
        try:
            t = type(config_file['Send Email']['From Name'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Sender Name in Config File')

        try:
            t = type(config_file['Send Email']['To Email'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Recipient Email in Config File')
        try:
            t = type(config_file['Send Email']['To Name'])
            self.assertTrue(t == str or t == unicode)
        except KeyError:
            self.fail('No Recipient Name in Config File')


class TestIMAP(unittest.TestCase):
    def test_login(self):
        params = digest.load_params()
        imap_instance = digest.IMAP(auto_login=False)
        self.assertTrue(imap_instance.login(params['IMAP Login']['username'], params['IMAP Login']['password']))

    def test_list_folders(self):
        imap_instance = digest.IMAP()
        folder_list = imap_instance.list_folders()
        self.assertTrue(len(folder_list) > 0, msg="No Folders Found in Mailbox")

    def test_select_folder(self):
        imap_instance = digest.IMAP()
        self.assertTrue(imap_instance.select_folder("INBOX"))

    def test_close(self):
        imap_instance = digest.IMAP()
        imap_instance.select_folder("INBOX")
        self.assertTrue(imap_instance.close())


if __name__ == '__main__':
    unittest.main()
