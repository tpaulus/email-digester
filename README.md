Email Digester
===============

Read one or more IMAP folders and send you a summary email every week showing you what emails were added to that folder were added that week. Very useful for monitoring a Spam or Junk mailbox.
