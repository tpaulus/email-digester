__author__ = 'tpaulus'

import atexit
import datetime
import email
import email.header
import imaplib
import json
import logging
import os
import sys
from logging.config import dictConfig

import requests
import yaml


def setup_logging(
        default_path='logging_config.yaml',
        default_level=logging.WARNING,
        env_key='LOG_CFG'):
    """
    Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def load_params():
    try:
        with open('email_config.json') as config_file:
            params = json.loads(config_file.read())

        logger.info("Config File Loaded")
        return params

    except IOError:
        logger.critical("Config File Not Found!")
        sys.exit(1)


def fix_encoding(s):
    return s.encode('utf8', 'ignore')


def fix_decoding(s):
    return s.decode('utf-8')


setup_logging()
logger = logging.getLogger(__name__)


class IMAP(object):
    def __init__(self, auto_login=True):
        """
        :param auto_login: Disable init from calling login, used for unittests
        """
        params = load_params()

        self.mailbox = imaplib.IMAP4_SSL(params['IMAP Login']['url'])
        if auto_login:
            self.login(params['IMAP Login']['username'], params['IMAP Login']['password'])

        atexit.register(IMAP.logout, self.mailbox)

    def login(self, username, password):
        """
        Login to the IMAP Server

        :param username: Username for Login
        :type username: str
        :param password: Password for given username for login
        :type password: str
        :return:
        """
        try:
            result, data = self.mailbox.login(username, password)
            logger.info("LOGIN %s", result)
            logger.debug('LOGIN Returned: %s', data)
            return True

        except imaplib.IMAP4.error, e:
            logger.critical('Login Problem - %s', e)
            return False

    def list_folders(self):
        """
        List all folders in Mailbox

        :return: Folders in Mailbox
        :rtype: list
        """
        result, folders = self.mailbox.list()
        logger.info('LIST FOLDERS %s', result)
        logger.debug('LIST FOLDERSReturned: %s', str(folders))

        if result == 'OK':
            return folders
        else:
            return False

    def select_folder(self, folder_name):
        """
        Select folder to perform operations in

        :param folder_name: Folder to Select
        :type folder_name: str
        """
        logger.info("Selecting Mailbox '%s'", folder_name)

        result, data = self.mailbox.select(folder_name)
        logger.info('SELECT MAILBOX %s %s', folder_name.upper(), result)
        logger.debug('SELECT MAILBOX Returned: %s', str(data))

        if result == 'OK':
            return True
        else:
            logger.error('ERROR: Unable to open mailbox %s' % result)
            return False

    def search_messages(self, query):
        """
        Search for messages that contain the supplied query in the selected mailbox.

        :param query: What to search for? 'ALL' will return all messages in that folder
        :type query: str
        :return: Message numbers that match the query
        :rtype: list
        """
        logger.info('Searching for messages that match: %s', query)

        result, data = self.mailbox.search(None, query)
        logger.info('SEARCH FOR %s %s', query.upper(), result)
        logger.debug('SEARCH FOR %s Returned: %s', query.upper(), data)

        split_list = data[0].split(' ')
        return_list = list()

        for i in split_list:
            return_list.append(int(i))

        if len(return_list) > 500 and query.upper() != 'ALL':
            # Suppress warning if fetching ids of all messages in a folder.
            logger.warning('Search returned more than 500 results, fetching all is not recommended.')

        return return_list

    def fetch_message(self, message_id):
        """
        Fetch a message from the IMAP Server.

        :param message_id: The Number of the message in the selected folder you want to fetch
        :type message_id: int
        :return: Message
        :rtype: email
        """
        logger.info("Fetching Message with ID: %i", message_id)

        result, data = self.mailbox.fetch(message_id, '(RFC822)')
        logger.info('FETCH MESSAGE %i %s', message_id, result)
        logger.debug('FETCH MESSAGE %i Returned: %s', message_id, data)

        if result == 'OK':
            return email.message_from_string(data[0][1])
        else:
            return False

    def close(self):
        """
        Close selected Folder. Required when you want to select a new folder.
        """
        self.mailbox.close()
        logger.info('MAILBOX CLOSE OK')
        return True

    @staticmethod  # Is static so it can be called after exit without self argument.
    def logout(mailbox):
        """
        Logout of Mailbox when done.
        """
        mailbox.logout()
        logger.info('LOGOUT OK')


class HTML(object):
    @staticmethod
    def folder(folder_name):
        base = """<h2 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
                    font-size: 24px; line-height: 1.2; color: #000; font-weight: 200; margin: 40px 0 10px; padding: 0;">
                                %s</h2>
                            <ul>
                            {{ messages }}
                            </ul>""" % folder_name

        logger.debug("HTML Block for Folder with name '%s':\n%s", folder_name, base)
        return base

    @staticmethod
    def row(msg_from, msg_subject):
        base = """<li style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px;
        line-height: 1; font-weight: normal; margin: 0 0 10px; padding: 0;">%s </b>-</b> %s</li>
        \n""" % (fix_decoding(msg_from), fix_decoding(msg_subject))

        logger.debug("HTML Block for message from '%s':\n%s", msg_from, base)
        return base

    @staticmethod
    def generate(email_by_folder_dict):
        insertion_module = ''

        for folder in email_by_folder_dict.keys():
            logger.info("Generating HTML Module for Folder with name '%s'", folder)
            top = HTML.folder(folder)
            emails = ''
            logger.info("There are %i emails in this folder that will be digested", len(email_by_folder_dict[folder]))
            for message in email_by_folder_dict[folder]:
                emails += HTML.row(message['from'], message['subject'])

            insertion_module += top.replace('{{ messages }}', emails)

        logger.debug('Insertion Module for Email has %i characters', len(insertion_module))

        with open('./Email Template.html') as template:
            email_html = template.read().replace('{{ generated_email_digest_content }}', insertion_module)
            header_date = datetime.datetime.now().strftime("%A, %B %d")
            if header_date[0] == '0':  # Remove preceding 0 on days less than 10.
                header_date = header_date[1]
            email_html = email_html.replace('{{ generated_on_date_header }}', header_date)
            email_html = email_html.replace('{{ generated_on_date_footer }}', datetime.datetime.now().isoformat())

        logger.debug('Final Email has %i characters.', len(email_html))

        with open('./Last Sent Email.html', 'w') as save_file:
            save_file.write(fix_encoding(email_html))

        logger.info("Email has been saved to './Last Sent Email.html' for reference.")

        return email_html

    def __init__(self):
        pass


class SendGrid(object):
    def __init__(self):
        self.params = load_params()
        self.key = self.params['Send Email']['Sendgrid']['API Key']

    def send(self, subject, message_html):
        data = dict()
        data['to'] = self.params['Send Email']['To Email']
        data['toname'] = self.params['Send Email']['To Name']
        data['from'] = self.params['Send Email']['From Email']  # Replace this with your from address
        data['fromname'] = self.params['Send Email']['From Name']
        data['subject'] = subject
        data['html'] = message_html

        session = requests.Session()
        session.headers.update({'Authorization': 'Bearer ' + self.key})
        sg_response = session.post('https://api.sendgrid.com/api/mail.send.json', data)

        json = sg_response.json()

        if json['message'] == 'success':
            return True, None
        else:
            return False, json['errors']


class Digest(object):
    @staticmethod
    def parse_email(msg):
        """
        Take in a Email Message and return a dict with the key info

        :param msg: Email to Process
        :type msg: email
        :return: Dictionary with important information from message
        :rtype: dict
        """
        email_info = dict()

        email_info['from'] = email.header.decode_header(msg['From'])[0][0]

        email_info['subject'] = email.header.decode_header(msg['Subject'])[0][0]

        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if date_tuple:
            date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
            email_info['date'] = date

        email_info['content'] = msg.get_payload()
        logger.info("Parsed email from: '%s' about: '%s'", fix_decoding(email_info['from']),
                    fix_decoding(email_info['subject']))
        logger.debug('Parsed Email Details: %s', str(email_info))
        return email_info

    def __init__(self):
        mailbox = IMAP()
        params = load_params()

        emails_by_folder = dict()
        for folder in params['Folders']:
            if not folder['Digest']:
                continue
            folder_name = folder['Name']
            emails_by_folder[folder_name] = list()

            mailbox.select_folder(folder_name)
            for m in reversed(mailbox.search_messages('All')):
                # Emails are saved Oldest to Newest
                fetched_message = Digest.parse_email(mailbox.fetch_message(m))

                try:
                    if datetime.datetime.now() - fetched_message['date'] > datetime.timedelta(folder['Digest After']):
                        logger.info("Stopping after %i messages - Emails are older than %i days.",
                                    len(emails_by_folder[folder_name]), folder['Digest After'])
                        break

                except KeyError:
                    logger.info("Email from: '%s'' in Folder: '%s' has NO Date", fetched_message['from'], folder_name)

                emails_by_folder[folder_name].append(fetched_message)

        html_for_email = HTML.generate(emails_by_folder)
        subject_date = datetime.datetime.now().strftime("%B %d")
        if subject_date[0] == '0':  # Remove preceding 0 on days less than 10.
            subject_date = subject_date[1]
        SendGrid().send("Email Digest for %s" % subject_date, html_for_email)

        logger.info("Digest Completed")


if __name__ == "__main__":
    Digest()
